DROP SCHEMA IF EXISTS `evaluacionrestDB` ;
CREATE SCHEMA IF NOT EXISTS `evaluacionrestDB` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_spanish_ci;
USE `evaluacionrestDB`;

DROP USER 'userrest'@'localhost';

CREATE USER 'userrest'@'localhost' IDENTIFIED BY 'Ab3456.';
GRANT ALL ON evaluacionrestDB.* TO 'userrest'@'localhost';

DROP TABLE IF EXISTS `evaluacionrestDB`.`articulos`; 
CREATE TABLE IF NOT EXISTS `evaluacionrestDB`.`articulos` 
(
  `IDARTICULO`          VARCHAR(10) NOT NULL,
  `NOMBRE`              VARCHAR(20) NOT NULL,
  `DESCRIPCION`         VARCHAR(200),
  `PRECIO`              DECIMAL(10,2) NOT NULL,
  `MODELO`              VARCHAR(10) NOT NULL,
  PRIMARY KEY (`IDARTICULO`)
  )
ENGINE = InnoDB;

INSERT INTO `evaluacionrestDB`.`articulos`  VALUES ('123456789A', 'papas', 'papas para comer', 23.67, 'sabritas');
INSERT INTO `evaluacionrestDB`.`articulos`  VALUES ('123456789B', 'radio', 'producto electronico', 200.67, 'sony');

commit;
