/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.chris.evaluacionrest.entity;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author christian.ventura
 */
@Entity
public class Articulos implements Serializable {
    
    @Id
    @Column(name="IDARTICULO")
    @ApiModelProperty(notes = "id del articulo", required = true)
    private String idArticulo;
    
    @Column(name="NOMBRE")
    @ApiModelProperty(notes = "nombre del articulo", required = true)
    private String nombre;
    
    @Column(name="DESCRIPCION")
    @ApiModelProperty(notes = "descripcion del articulo")
    private String descripcion;
    
    @Column(name="PRECIO")
    @ApiModelProperty(notes = "precio del articulo", required = true)
    private BigDecimal precio;
    
    @Column(name="MODELO")
    @ApiModelProperty(notes = "modelo del articulo", required = true)
    private String modelo;

    /**
     * @return the idArticulo
     */
    public String getIdArticulo() {
        return idArticulo;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the precio
     */
    public BigDecimal getPrecio() {
        return precio;
    }

    /**
     * @return the modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * @param modelo the modelo to set
     */
    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
    
    
}
