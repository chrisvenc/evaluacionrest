/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.chris.evaluacionrest.services;

import mx.com.chris.evaluacionrest.entity.Articulos;

/**
 *
 * @author christian.ventura
 */
public interface ArticulosService {
    
    /**
     * 
     * @param id
     * @return 
     */
    Articulos getArticuloByID(String id);
    
    /**
     * 
     * @param articulo
     * @return 
     */
    Articulos actualizaArtuculo(Articulos articulo);
    
}
