/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.chris.evaluacionrest.services.impl;

import mx.com.chris.evaluacionrest.entity.Articulos;
import mx.com.chris.evaluacionrest.repositories.ArticulosRepository;
import mx.com.chris.evaluacionrest.services.ArticulosService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author christian.ventura
 */
@Service
public class ArticulosServiceImpl implements ArticulosService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    
    @Autowired
    private ArticulosRepository articulosRepository;
    
    /**
     * 
     * @param id
     * @return 
     */
    @Transactional
    @Override
    public Articulos getArticuloByID(String id) {
        logger.debug("getArticuloByID");
        return articulosRepository.findById(id).orElse(null);
    }

    /**
     * 
     * @param articulo
     * @return 
     */
    @Transactional
    @Override
    public Articulos actualizaArtuculo(Articulos articulo) {
        logger.debug("actualizaArtuculo");
        return articulosRepository.save(articulo);
    }
    
}
