/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.chris.evaluacionrest.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import mx.com.chris.evaluacionrest.entity.Articulos;
import mx.com.chris.evaluacionrest.services.ArticulosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author christian.ventura
 */
@RestController
@RequestMapping("/articulos")
@Api(value="consultaarticulos", description="manejo de articulos de una empresa")
public class ArticulosController {
    
    @Autowired
    private ArticulosService articulosService;
    
    /**
     * 
     * @param id
     * @param model
     * @return 
     */
    @ApiOperation(value = "busca un producto por id",response = Articulos.class)
    @RequestMapping(value = "/buscar/{id}", method= RequestMethod.GET, produces = "application/json")
    public Articulos getArticuloByID(@PathVariable String id, Model model){
       Articulos product = articulosService.getArticuloByID(id);
        return product;
    }
    
    /**
     * 
     * @param id
     * @param articulo
     * @return 
     */
    @ApiOperation(value = "actualizar un articulo")
    @RequestMapping(value = "/actualizar/{id}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity updateProduct(@PathVariable String id, @RequestBody Articulos articulo){
        Articulos articuloTemp = articulosService.getArticuloByID(id);
        articuloTemp.setDescripcion(articulo.getDescripcion());
        articuloTemp.setModelo(articulo.getModelo());
        articulosService.actualizaArtuculo(articuloTemp);
        return new ResponseEntity("se actualizo el articulo", HttpStatus.OK);
    }
    
}
