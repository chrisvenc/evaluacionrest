/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.chris.evaluacionrest.repositories;

import mx.com.chris.evaluacionrest.entity.Articulos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 *
 * @author christian.ventura
 */
@RepositoryRestResource
public interface ArticulosRepository extends JpaRepository<Articulos, String> {
    
}
